package com.epam.controller;

import com.epam.model.Model;
import com.epam.model.Product;
import com.epam.model.ProductListLogic;

import java.util.List;

public class MyController implements Controller {

    private Model model;

    public MyController() {
        model = new ProductListLogic();
    }

    @Override
    public void addNewProduct(String name, int price, int amount, int id) {
        model.addNewProduct(name, price, amount, id);
    }

    @Override
    public List<Product> getProductList() {
        return model.getProductList();
    }

    @Override
    public List<Product> sortProductListByPrice() {
        return model.sortProductListByPrice();
    }

    @Override
    public List<Product> sortProductListByAmount() {
        return model.sortProductListByAmount();
    }

    @Override
    public void createProductFile() {
        model.createProductFile();
    }
}

package com.epam.model;

import java.util.List;

public class ProductListLogic implements Model {

    private Product product;

    public ProductListLogic() {
        product = new Product();
    }

    @Override
    public void addNewProduct(String name, int price, int amount, int id) {
        product.addNewProduct(name, price, amount, id);
    }

    @Override
    public List<Product> getProductList() {
        return product.getProductList();
    }

    @Override
    public List<Product> sortProductListByPrice() {
        return product.sortProductListByPrice();
    }

    @Override
    public List<Product> sortProductListByAmount() {
        return product.sortProductListByAmount();
    }

    @Override
    public void createProductFile() {
        product.createProductFile();
    }
}

package com.epam.model;

import java.util.List;

public interface Model {

    void addNewProduct(String name, int price, int amount, int id);

    List<Product> getProductList();

    List<Product> sortProductListByPrice();

    List<Product> sortProductListByAmount();

    void createProductFile();
}
